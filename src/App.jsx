import React, { useState, useEffect, useContext } from "react";
import { BrowserRouter as Router, Routes, Route } from "react-router-dom";
import Nav from "./components/Navbar/Nav";
import Data from "./components/CountryData/Data";
import Search from "./components/Search/Search";
import CountryDetail from "./components/CountryDetail/CountryDetail";
import { Theme } from "./components/Context";

export default function App() {
  const [allCountries, setAllCountries] = useState([]);
  const [filteredCountries, setFilteredCountries] = useState([]);
  const [region, setRegion] = useState("all");
  const [subregion, setSubRegion] = useState("all");
  const [sort, setSort] = useState("");
  const [currency,setCurrency]=useState("all")

  
  useEffect(() => {
    fetch("https://restcountries.com/v3.1/all")
      .then((response) => response.json())
      .then((data) => {
        setAllCountries(data);
        setFilteredCountries(data);
      });
  }, []);

  const onSearch = (value) => {
    const filteredData = allCountries.filter((country) => {
      return country.name.common.toLowerCase().startsWith(value.toLowerCase());
    });

    setFilteredCountries(filteredData);
  };

  const {darkmode}=useContext(Theme)

  return (
    <Router >
      <div className={`${darkmode ? "dark" : "light"}`}>
        <Nav />
        <Routes>
          <Route path="/" element={
            <div>
              <Search
                setSort={setSort}
                region={region}
                setRegion={setRegion}
                onSearch={onSearch}
                allCountries={allCountries}
                setAllCountries={setAllCountries}
                subregion={subregion}
                setSubRegion={setSubRegion}

                currency={currency}
                setCurrency={setCurrency}
              />
              <Data
                countries={filteredCountries}
                region={region}
                subregion={subregion}
                sort={sort}
                currency={currency}
              />
            </div>
          } />
          <Route path="/country/:cca3" element={<CountryDetail />} />
        </Routes>
      </div>
    </Router>
  );
}
