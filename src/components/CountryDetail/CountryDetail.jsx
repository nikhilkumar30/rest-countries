import React, { useState, useEffect } from "react";
import { useParams, Link } from "react-router-dom";
import { Theme } from "../Context";
import { useContext } from "react";
import { AiOutlineArrowLeft } from "react-icons/ai";
import "./CountryDetail.css";

const CountryDetail = () => {
  const [currentCountry, setCurrentCountry] = useState(null);
  const [error, setError] = useState(false);

  const { darkmode } = useContext(Theme);
  const { cca3 } = useParams();

  useEffect(() => {
    fetch(`https://restcountries.com/v3.1/alpha/${cca3}`)
      .then((response) => response.json())
      .then((data) => {
        setCurrentCountry(data[0]);
      })
      .catch((error) => {
        setError(true);
        console.log(error);
      });
  }, [cca3]);

  if (currentCountry === null) {
    return <div>Loading...</div>;
  }

  return (
    <div className={`detailBox ${darkmode ? "dark" : "light"}`}>
      <Link to="/">
        <button className={`btn backBtn ${darkmode ? "light" : "light"}`}>
          <span>
            <AiOutlineArrowLeft />
          </span>{" "}
          Back
        </button>
      </Link>

      {!error && currentCountry && (
        <div className="countryDetail">
          <div className="box1">
            <img src={currentCountry.flags.png} alt="Flag" />
          </div>

          <div className="infoContainer">
            <div className="box2">
              <h3>Country: {currentCountry.name.common}</h3>
            </div>
            <div className="countryNamestuff box2">
              <div className="box3">
                <p>
                  Native Name:
                  {
                    currentCountry.name.nativeName && currentCountry.name.nativeName[
                      Object.keys(currentCountry.name.nativeName)[0]
                    ].common
                  }
                </p>
                <p>Population: {currentCountry.population}</p>
                <p>Region: {currentCountry.region}</p>
                <p>Capital: {currentCountry.capital}</p>
              </div>

              <div className="box3">
                <p>
                  Top Level Domain:
                  {currentCountry.tld}
                </p>
                <p>
                  Currencies:
                  {Object.keys(currentCountry.currencies)}
                </p>

                <p>
                  Languages:
                  {Object.values(currentCountry.languages).join(",")}
                </p>
              </div>
            </div>

            <div className="bondries box2">
              <h5>Borders Countries: </h5>
              {currentCountry.borders &&
                currentCountry.borders.map((borderCountryCode) => (
                  <Link to={`/country/${borderCountryCode}`} key={borderCountryCode}>
                    <button className={`btn ${darkmode ? "light" : "light"}`} >
                      {borderCountryCode}
                    </button>
                  </Link>
                ))}
            </div>
          </div>
        </div>
      )}
    </div>
  );
};

export default CountryDetail;
