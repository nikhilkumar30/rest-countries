import React, { createContext, useState } from "react";

const Theme = createContext({});

const Context = ({ children }) => {
  const [darkmode, setDarkMode] = useState(false);

  const toggleTheme = () => {
    setDarkMode((pre) => !pre);
  };

  return (
    <div>
      <Theme.Provider value={{ darkmode, toggleTheme }}>
        {children}
      </Theme.Provider>
    </div>
  );
};

export { Theme };
export default Context;
