import React, { useContext, useState } from "react";
import "./search.css";
import { AiOutlineSearch } from "react-icons/ai";
import { Theme } from "../Context";

export default function Search({
  region,
  setRegion,
  allCountries,
  onSearch,
  setSubRegion,
  setSort,
  setCurrency
}) {
  const { darkmode } = useContext(Theme);
  const [selectedOption, setSelectedOption] = useState("all");
  const [sortOption, setSortOption] = useState("all");

  const regions = {};
  const currencies = {};

  // console.log(currencies);

  function handleRegionChange(inputValue) {
    setRegion(inputValue);
    setSubRegion("all");
    setSortOption("all");
  }

  allCountries.forEach((country) => {
    if (!regions[country.region]) {
      regions[country.region] = [];
    }
    if (!regions[country.region].includes(country.subregion)) {
      regions[country.region].push(country.subregion);
    }

    if (country.currencies) {
      Object.keys(country.currencies).forEach((key) => {
        currencies[key] = country.currencies[key].name;
      });
    }
  });

  function handleSortClick(input) {
    setSort(input);
  }

  function handleCurrencyClick(input) {
    setCurrency(input);
  }

  return (
    <div className={`main-container ${darkmode ? "dark" : "light"}`}>
      <form>
        <div className={`search-container ${darkmode ? "light" : "light"}`}>
          <AiOutlineSearch size={28} fontWeight={500} />
          <input
            type="text"
            id="search"
            className="search-input"
            placeholder="Search for a country..."
            onChange={(e) => {
              onSearch(e.target.value);
            }}
          />
        </div>
      </form>

      <div className="dropdown">
        <select
          onChange={(e) => {
            handleCurrencyClick(e.target.value);
            setCurrency(e.target.value);
          }}
        >
          <option value="all">Filter By Currency</option>
          {Object.values(currencies).map((currency, j) => (
            <option key={j} value={currency}>
              {currency}
            </option>
          ))}
        </select>
      </div>

      <div className="dropdown">
        <select
          onChange={(e) => {
            handleSortClick(e.target.value);
            setSortOption(e.target.value);
          }}
          value={sortOption}
        >
          <option value="all">Sort By</option>
          <optgroup label="Population">
            <option value="population_Ascending">
              Population in Ascending
            </option>
            <option value="population_Descending">
              Population in Descending
            </option>
          </optgroup>
          <optgroup label="Area">
            <option value="area_Ascending">Area in Ascending</option>
            <option value="area_Descending">Area in Descending</option>
          </optgroup>
        </select>
      </div>

      <div className="dropdown">
        <select
          onChange={(e) => {
            handleRegionChange(e.target.value);
            setSubRegion("all");
            setSelectedOption("all");
          }}
        >
          <option value="all">Filter By Region</option>
          {Object.keys(regions).map((region, i) => (
            <option key={i} value={region}>
              {region}
            </option>
          ))}
        </select>
      </div>

      <div className="dropdown">
        <select
          onChange={(e) => {
            setSubRegion(e.target.value);
            setSelectedOption(e.target.value);
          }}
          value={selectedOption}
        >
          <option value="all">Filter By Subregion</option>
          {region !== "all" &&
            regions &&
            regions[region].map((subregion, i) => (
              <option key={i} value={subregion}>
                {subregion}
              </option>
            ))}
        </select>
      </div>
    </div>
  );
}
