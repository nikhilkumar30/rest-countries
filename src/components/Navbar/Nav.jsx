import React, { useContext } from "react";
import "./Nav.css";
import { FiMoon } from "react-icons/fi";
import { Theme } from "../Context";

export default function Nav() {
  const { darkmode, toggleTheme } = useContext(Theme);

  return (
    <div className={`box headcontent ${darkmode ? "dark":"light"}`}>
      <div className="title">
        <h1 className={darkmode ? "dark":"light"}>Where in the world?</h1>
        {/* <p>{JSON.stringify(darkmode)}</p> */}
      </div>
      <div className={darkmode ? "dark" : "light"}>
        <button className={`toggle ${darkmode ? "dark":"light"}`} onClick={() => toggleTheme()}>
          <FiMoon size={32} />
          
          <span className={`modeTitle ${darkmode?  "dark" : "light"}`}>{darkmode?  'Light Mode' :'Dark Mode'}</span>
        </button>
      </div>
    </div>
  );
}
