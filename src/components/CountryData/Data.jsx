import React, { useContext } from "react";
import "./Data.css";
import { Theme } from "../Context";
import { Link } from "react-router-dom";

export default function Data({ countries, region, subregion, sort, currency }) {
  const { darkmode } = useContext(Theme);

  const filteredCountries = countries.filter((country) => {

    let curr=[]
    if (country.currencies) {
      curr=Object.values(country.currencies).reduce((acc,cr)=>{
        acc.push(cr.name)
        return acc
      },[])

    }
    return (
      (region === "all" ||
        country.region.toLowerCase() === region.toLowerCase()) &&
      (subregion === "all" ||
        country.subregion.toLowerCase() === subregion.toLowerCase()) &&
      (currency == "all" || !curr|| curr.includes(currency))    
    );
  });

  if (sort) {
    let sortGroup = sort.split("_");
    filteredCountries.sort((a, b) => a[sortGroup[0]] - b[sortGroup[0]]);
    if (sortGroup[1] === "Descending") {
      filteredCountries.reverse();
    }
  }
  
  return (
    <div className={`container ${darkmode ? "dark" : "light"}`}>
      {filteredCountries.map((country) => (
        <Link
          to={`/country/${country.cca3}`}
          key={country.cca3}
          style={{ textDecoration: "none" }}
          className={`${darkmode ? "dark" : "light"}`}
        >
          <div className="innerContainer">
            <img src={`${country.flags.png}`} alt="" />
            <div className="content">
              <h4 className="countryName">{country.name.common}</h4>
              <div className="countryDetails">
                <p>
                  Population :
                  <span className="population">{country.population}</span>
                </p>
                <p>
                  Region : <span className="region">{country.region}</span>
                </p>
                <p>
                  Capital : <span className="capital">{country.capital}</span>
                </p>
                <p>
                  Currency :
                  <span className="currency">
                    {country.currencies &&
                      Object.keys(country.currencies).map((name) => (
                        <span key={name}>{country.currencies[name].name}</span>
                      ))}
                  </span>
                </p>
              </div>
            </div>
          </div>
        </Link>
      ))}
      {filteredCountries.length === 0 && <h1>No Countries Found</h1>}
    </div>
  );
}
